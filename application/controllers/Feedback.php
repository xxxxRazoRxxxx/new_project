<?php


class Feedback extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('feedback_model');
		$this->load->helper('url_helper');
	}

	public function send()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Send call request';

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('feedback/send');
			$this->load->view('templates/footer');
		}
		else
		{
			$this->feedback_model->send_feedback();
			$this->load->view('feedback_model/success');
		}
	}
}
