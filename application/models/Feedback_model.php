<?php


class Feedback_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function send_feedback()
	{
		$this->load->helper('url');

		$data = [
			'name' => $this->input->post('name'),
			'message' => $this->input->post('message'),
		];

		$priority = $this->input->post('priority');

		if (!empty($priority) && $priority == 'urgent') {
			$this->load->library('email');

			$this->email->from('', $data['name']);
			$this->email->to('asamoylenko@cs-cart.com');

			$this->email->subject('Feedback');
			$this->email->message($data['message']);

			$this->email->send();
		} else {
			$this->db->insert('feedback', $data);
		}

		return;
	}
}
